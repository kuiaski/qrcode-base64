const QRCode = require('qrcode');
const { uuid } = require('uuidv4');
// Se ao invés de salvar os QRCodes na pasta qrcodes você quiser salvar na pasta
// temporária do sistema, tem que usar o módulo os
// const os = require('os');
const path = require('path');
const fs = require('fs');

// Aqui a pasta QRCodes tá apontando pra pasta
// ./qrcodes, no root da solução. Se quiser apontar
// pra pasta temporária do sistema, tem que usar: 
//
// const qrCodesFolder = os.tmpdir();
const qrCodesFolder = './qrcodes';

// Gerando um arquivo com nome genérico
// pra evitar conflito de nome de arquivo.
const tmpFileName = `${uuid()}.png`;
const tmpFilePath = path.join(qrCodesFolder, tmpFileName);

// INPUTS

// O que vai ser criado como QRCode
const inputValue = 'Help! My hovercraft is full of eels';

// Vai ser gerado um arquivo .png dentro da pasta "qrCodesFolder".
// Pra desenvolvimento às vezes é uma boa deixar os arquivos lá
// pra verificar se tudo ocorreu direitinho
// MAS pra produção é melhor não deixar (pode consumir storage desnecessariamente!)
const shouldDeleteFileAfterCreation = false;

// Imagem será criada do tipo PNG
const options = {
    type: 'png'
}

QRCode.toFile(tmpFilePath, inputValue, options, (error) => {
    if (error) {
        console.error(error.toString());
    } else {
        // Se a imagem foi criada!
        const data = fs.readFileSync(tmpFilePath);

        // Ler como bas64
        const base64 = data.toString('base64');

        // Se for pra apagar, esta é a hora!
        if (shouldDeleteFileAfterCreation) {
            fs.unlinkSync(tmpFilePath);
        }

        // Printando no console. Mas com esse bas64 vc faz o que quiser!
        console.log(base64);
    }
});
