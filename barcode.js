const JsBarcode = require('jsbarcode');
const { Canvas } = require("canvas");
const fs = require('fs');
const path = require('path');
const { uuid } = require('uuidv4');
 
const shouldDeleteFileAfterCreation = false;
const input = '1234567890';

const canvas = new Canvas();
JsBarcode(canvas, input, {
    format: 'CODE128'
});


const outDir = './barcodes';
const outFileName = `${uuid()}.png`
const outFile = path.resolve(outDir, outFileName);

const writeStream = fs.createWriteStream(outFile);
const stream = canvas.createPNGStream({
    resolution: 300
});
stream.pipe(writeStream);

writeStream.on('finish', () =>  {
    // Se a imagem foi criada!
    const data = fs.readFileSync(outFile);

    // Ler como bas64
    const base64 = data.toString('base64');

    // Se for pra apagar, esta é a hora!
    if (shouldDeleteFileAfterCreation) {
        fs.unlinkSync(tmpFilePath);
    }

    // Printando no console. Mas com esse bas64 vc faz o que quiser!
    console.log(base64);
});